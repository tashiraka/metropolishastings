/*
 * Simulation of polymer conformations in a equilibrium cannonical ensemble 
 * by Metropolis Monte Carlo method. 
 *
 * Usage:
 *     ./<This program> <JSON file> <>
 */
import std.stdio;
import std.file;
import std.json;
import std.array;
import std.getopt;
import std.conv;
import std.random;
import core.stdc.stdlib;
import std.math;
import std.algorithm;
import std.typecons;


void usage() {
  writeln(r"
 DESCRIPTION:
     This command is a tool to 

 USAGE:
     command [-vh] <in FASTQ> <out FASTQ>
 
 ARGMENTS:
     in FASTQ
         FASTQ file for input.
     out FASTQ
         FASTQ file for output.

 OPTIONS:
     -v
         print version.
     -h
         print this.
 
 AUTHOR:
     written by Yuichi Motai.
");

  exit(0);
}


void thisVersion() {
  writeln("version 1.0");
  exit(0);
}


/*-------------main----------------*/
version(unittest) {}
 else {
   void main(string[] args) {
     /*--------parsing option---------------*/
     try {
       getopt(args, "version|v", &thisVersion, "help|h", &usage);
       if(args.length != 3)
         throw new Exception("ERROR: the number of argments illeagal.");
     }
     catch(GetOptException e) {
       writeln("ERROR: invalid option\n", e.toString);
       usage();
     }
     catch(Exception e) {
       writeln(e.toString);
       usage();
     }

     auto jsonFile = args[1];
     auto json = parseJSON(readText(jsonFile));

     auto samplingSize = json.object["samplingSize"].integer;
     auto polymerSize = json.object["polymerSize"].integer;
     auto bondLength = json.object["bondLength"].FLOAT;
     auto excludedRadii = json.object["excludedRadii"].array;
     auto boundarySphereRadii = json.object["boundarySphereRadii"].array;
     auto stepSizes = json.object["stepSizes"].array;
     
     auto ensemble = new Ensemble;
     for(auto i=0; i<samplingSize; i++)
       ensemble.put(polymerSimulation(polymerSize,
                                      bondLength,
                                      excludedRadii,
                                      boundarySphereRadii,
                                      stepSizes));     
   }
 }


class Point
{
private:
  real x_=0.0, y_=0.0, z_=0.0;
  
public:
  this(real x, real y, real z) { x_=x; y_=y; z_=z; }
  
  @property {
    real x() { return x_; }
    real y() { return y_; }
    real z() { return z_; }
  }

  Point opBinary(string op)(Point p) {
    static if(op == "+") return new Point(x_ + p.x, y_ + p.y, z_ + p.z);
    else static if(op == "-") return new Point(x_ - p.x, y_ - p.y, z_ - p.z);
    else static assert(0, "Operator "~op~" not implemented");
  }

  Point opBinary(string op)(real a) {
    static if(op == "/") return new Point(x_ / a, y_ / a, z_ / a);
    else static if(op == "*") return new Point(x_ * a, y_ * a, z_ * a);
    else static assert(0, "Operator "~op~" not implemented");
  }

  Point opBinaryRight(string op)(real a) {
    static if(op == "/") return new Point(x_ / a, y_ / a, z_ / a);
    else static if(op == "*") return new Point(x_ * a, y_ * a, z_ * a);
    else static assert(0, "Operator "~op~" not implemented");
  }
}


class Polymer
{
private:
  Point[] points_;
  bool[] outsideSpheres_;
  real energy_;
  real bondLength_;
  real boundarySphereRadius_;

  struct PhantomMoveCandidate {
    ulong decrement = 0;
    Point[] points = [];
    ulong[] indeces = [];
    bool[] outsideSpheres = [];
  }
  
  /*------3 elemental moves (local moves)------*/
  Tuple!(Point[], "points", ulong[], "indeces") endBondMove(ulong i) {
    if(i == 0)
      return Tuple!(Point[], "points", ulong[], "indeces")
                   ([points_[i+1] + randomPointOnSphere(bondLength_)], [i]);
    else
      return Tuple!(Point[], "points", ulong[], "indeces")
                   ([points_[i-1] + randomPointOnSphere(bondLength_)], [i]);
  }

  Tuple!(Point[], "points", ulong[], "indeces") crankShaftMove(ulong i) {
    /*circle in the space spaned by vector (points_[i] - middlePoint) 
      and vector orthogonalPoint*/
    auto middlePointOfAxis = (points_[i-1] + points_[i+1]) / 2;
    auto axis = points_[i + 1] - middlePointOfAxis;
    auto base = points_[i] - middlePointOfAxis;
    auto orthogonalBase = outerProduct(base, axis);
    orthogonalBase = orthogonalBase
      * euclideanNorm(points_[i] - middlePointOfAxis)
      / euclideanNorm(orthogonalBase); 
    auto components = randomPointOnCircleXY(1);
    auto crankShaftPoint = middlePointOfAxis +
      components[0] * base + components[1] * orthogonalBase;
    return Tuple!(Point[], "points", ulong[], "indeces")([crankShaftPoint], [i]);
  }

  Tuple!(Point[], "points", ulong[], "indeces") threeConsecutiveMove(ulong i) {
    
    return new Point;
  }

  /*------1 grobal move-----------------------*/
  Tuple!(Point[], "points", ulong[], "indeces") pivotMove(ulong i) {
    return new Point[3];
  }

  PhantomMoveCandidate
  calcCandidatePhantomMove(ulong i,
                           Tuple!(Point[], "points", ulong[], "indeces")
                           delegate(ulong i) move)
  {
    auto displacedPoints = move(i);
    auto outsideSpheres
      = displacedPoints.points.map!(x => euclideanNorm(x) > boundarySphereRadius_);
    auto decrement
      = reduce!((a,b) => a + (b ? 1 : 0))(0.0, outsideSpheres)
      - outsideSpheres_[displacedPoints.indeces[0]..displacedPoints.indeces[$-1]+1].reduce!((a,b) => a + b ? 1 : 0);
    
    if(decrement <= 0) 
      return PhantomMoveCandidate(decrement, displacedPoints.points,
                                  displacedPoints.indeces, outsideSpheres);
    else
      return PhantomMoveCandidate();
  }
  

public:
  this(Point[] p, real bondLength, real radius) {
    points_ = p;
    bondLength_ = bondLength;
    boundarySphereRadius_ = radius;
  }

  @property {
    real boundarySphereRadius() { return boundarySphereRadius_; }
    real boundarySphereRadius(real radius) { return boundarySphereRadius_ = radius; }
  }
  
  auto calcOutsideSphere() {
    outsideSpheres_ = points_.map!(x => euclideanNorm(x) > boundarySphereRadius_).array;
    return outsideSpheres_.count!(x => x);
  }

  // If the number of granules outside sphere decrease or is equal,
  // this function returns the decrement.
  auto phantomMove()
  {
    PhantomMoveCandidate[] candidates;
    auto i = uniform(0, points_.length);

    if(i == 0 || i == points_.length - 1) 
      candidates ~= calcCandidatePhantomMove(i, &endBondMove);
    else {
      // push by order of priority
      candidates ~= calcCandidatePhantomMove(i, &pivotMove);
      candidates ~= calcCandidatePhantomMove(i, &threeConsecutiveMove);
      candidates ~= calcCandidatePhantomMove(i, &crankShaftMove);
    }

    auto acceptedCandidate = candidates.minPos!((a,b) =>
                                                a.decrement < b.decrement)[0];
    foreach(j, noUse; acceptedCandidate.points) {
      points_[acceptedCandidate.indeces[j]] = acceptedCandidate.points[j];
      outsideSpheres_[acceptedCandidate.indeces[j]]
        = acceptedCandidate.outsideSpheres[j];
    }
    return acceptedCandidate.decrement;
  }
}



class Ensemble
{
  private Polymer[] _polymers;

  void put(Polymer polymer) {
    _polymers ~= polymer;
  }
}


Point randomPointInSphere(real radius)
{
  auto s = uniform!("[]")(real(0.0), radius);
  auto t = uniform!("[]")(real(0.0), real(1.0));
  auto u = uniform(real(0.0), real(2.0)*PI);
  return new Point(cbrt(s) * sqrt(1 - t*t) * cos(u),
                   cbrt(s) * sqrt(1 - t*t) * sin(u),
                   cbrt(s) * t);
}


Point randomPointOnSphere(real radius)
{
  auto t = uniform!("[]")(real(0.0), real(1.0));
  auto u = uniform(real(0.0), real(2.0)*PI);
  return new Point(radius * sqrt(1 - t*t) * cos(u),
                   radius * sqrt(1 - t*t) * sin(u),
                   radius * t);
}


Tuple!(real, real) randomPointOnCircleXY(real radius)
{
  auto u = uniform(real(0.0), real(2.0)*PI);
  return tuple(radius * cos(u),
               radius * sin(u));
}


Point outerProduct(Point a, Point b) {
  return new Point(a.y * b.z - a.z * b.y,
                   a.z * b.x - a.x * b.z,
                   a.x * b.y - a.y * b.x);
}


// init polymer associated with a shere
Polymer initPolymer(real polymerSize, real bondLength, real radius)
{
    auto pointApp = appender!(Point[]);

    // first granule in the sphere
    auto newPoint = randomPointInSphere(radius);
    pointApp.put(newPoint);

    // other granules consecutively
    foreach(i; 1..polymerSize) {
      newPoint = randomPointOnSphere(bondLength) + newPoint;
      pointApp.put(newPoint);
    }
    
    return new Polymer(pointApp.data, bondLength, radius);
}


real euclideanNorm(Point p)
{
  return sqrt(p.x^^2 + p.y^^2 + p.z^^2);
}


/*-----Metropolis Monte carlo simulation of equilibrium polymer-----------*/
Polymer polymerSimulation(uint polymerSize, real bondLength, real[] excludedRadii,
                          real[] boundarySphereRadii, real[] stepSizes)
{ 
  if(boundarySphereRadii.length != 2)
    throw new Exception("ERROR: boundarySphereRadii length != 2.");
  auto R0 = boundarySphereRadii[0];
  auto R1 = boundarySphereRadii[1];

  if(stepSizes.length != 4)
    throw new Exception("ERROR: stepSizes length != 4.");
  auto confineStepSize = stepSizes[0];
  auto phantomStepSize = stepSizes[1];
  auto exclutionStepSize = stepSizes[2];
  auto equilibriumStepSize = stepSizes[3];
  
  /*-----structual initialization------*/
  auto polymer = initPolymer(polymerSize, bondLength, R0);
  
  /*----compress polymer within the spherical confinement----*/
  auto count = polymer.calcOutsideSphere;
  auto step = 0;
  while(count > 0 || step < confineStepSize) {
    count -= polymer.phantomMove;
    step++;
  }
  if(count > 0)
    writeln("Warning: the number of granules outside the sphere: ", count);
  
  // equilibration of phantom polymer in the spherical cage.
  // turn on the granule excluded volume gradually
  // equibration of the polymer with excluded volume
  return polymer;
}
